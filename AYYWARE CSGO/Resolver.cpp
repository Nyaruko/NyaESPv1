#include "Resolver.h"
#include "Ragebot.h"
#include "Hooks.h"
#include "ESP.h"
#include "Interfaces.h"
#include "RenderManager.h"
#include "GlowManager.h"
#include "Autowall.h"
#include <stdio.h>
#include <stdlib.h>
#include "EdgyLagComp.h"









void PitchCorrection()
{
	CUserCmd* pCmd;
	for (int i = 0; i < Interfaces::Engine->GetMaxClients(); ++i)
	{
		IClientEntity* pLocal = hackManager.pLocal();
		IClientEntity *player = (IClientEntity*)Interfaces::EntList->GetClientEntity(i);

		if (!player || player->IsDormant() || player->GetHealth() < 1 || (DWORD)player == (DWORD)pLocal)
			continue;

		if (!player)
			continue;

		if (pLocal)
			continue;

		if (pLocal && player && pLocal->IsAlive())
		{
			if (Menu::Window.RageBotTab.AdvancedResolver.GetState())
			{
				Vector* eyeAngles = player->GetEyeAnglesXY();
				if (eyeAngles->x < -179.f) eyeAngles->x += 360.f;
				else if (eyeAngles->x > 90.0 || eyeAngles->x < -90.0) eyeAngles->x = 89.f;
				else if (eyeAngles->x > 89.0 && eyeAngles->x < 91.0) eyeAngles->x -= 90.f;
				else if (eyeAngles->x > 179.0 && eyeAngles->x < 181.0) eyeAngles->x -= 180;
				else if (eyeAngles->x > -179.0 && eyeAngles->x < -181.0) eyeAngles->x += 180;
				else if (fabs(eyeAngles->x) == 0) eyeAngles->x = std::copysign(89.0f, eyeAngles->x);
			}
		}
	}
}



void ResolverSetup::Resolve(IClientEntity* pEntity)
{
	bool MeetsLBYReq;
	if (pEntity->GetFlags() & FL_ONGROUND)
		MeetsLBYReq = true;
	else
		MeetsLBYReq = false;

	bool IsMoving;
	if (pEntity->GetVelocity().Length2D() >= 0.5)
		IsMoving = true;
	else
		IsMoving = false;

	ResolverSetup::NewANgles[pEntity->GetIndex()] = *pEntity->GetEyeAnglesXY();
	ResolverSetup::newlby[pEntity->GetIndex()] = pEntity->GetLowerBodyYaw();
	ResolverSetup::newsimtime = pEntity->GetSimulationTime();
	ResolverSetup::newdelta[pEntity->GetIndex()] = pEntity->GetEyeAnglesXY()->y;
	ResolverSetup::newlbydelta[pEntity->GetIndex()] = pEntity->GetLowerBodyYaw();
	ResolverSetup::finaldelta[pEntity->GetIndex()] = ResolverSetup::newdelta[pEntity->GetIndex()] - ResolverSetup::storeddelta[pEntity->GetIndex()];
	ResolverSetup::finallbydelta[pEntity->GetIndex()] = ResolverSetup::newlbydelta[pEntity->GetIndex()] - ResolverSetup::storedlbydelta[pEntity->GetIndex()];
	if (newlby == storedlby)
		ResolverSetup::lbyupdated = false;
	else
		ResolverSetup::lbyupdated = true;

	if (Menu::Window.RageBotTab.AimbotResolver.GetIndex() == 0)
	{
		resolvokek::resolvemode = 0;
	}
	
		 if (Menu::Window.RageBotTab.AimbotResolver.GetIndex() == 1) //level 2 
	{
		resolvokek::resolvemode = 1;
		if (Resolver::didhitHS)
		{
			if (MeetsLBYReq && lbyupdated)
			{
				
				pEntity->GetEyeAnglesXY()->y = pEntity->GetLowerBodyYaw();
			}
			switch (Globals::Shots % 6)
			{
			case 1:
				*pEntity->GetEyeAnglesXY() = StoredAngles[pEntity->GetIndex()];
				break;
			case 2:
				*pEntity->GetEyeAnglesXY() = StoredAngles[pEntity->GetIndex()];
				break;
			case 3:
				pEntity->GetEyeAnglesXY()->y = pEntity->GetLowerBodyYaw() - 15;
				break;
			case 4:
				pEntity->GetEyeAnglesXY()->y = pEntity->GetEyeAnglesXY()->y + 40;
				break;
			case 5:
				pEntity->GetEyeAnglesXY()->y = pEntity->GetEyeAnglesXY()->y + 15;
				break;
			case 6:
				pEntity->GetEyeAnglesXY()->y = pEntity->GetEyeAnglesXY()->y - 40;
				break;
			}
		}
		else if (MeetsLBYReq && lbyupdated && !Resolver::didhitHS)
		{
			pEntity->GetEyeAnglesXY()->y = pEntity->GetLowerBodyYaw();
		}
		else if (!MeetsLBYReq || !lbyupdated && !Resolver::didhitHS)
		{
			pEntity->GetEyeAnglesXY()->y = rand() % 180 - rand() % 35;
		}
		else
			pEntity->GetEyeAnglesXY()->y = rand() % 180;
	}
	else if (Menu::Window.RageBotTab.AimbotResolver.GetIndex() == 2)//level 3
	{
		resolvokek::resolvemode = 2;
		if (Globals::missedshots > 3 && Globals::missedshots < 21)
		{
			if (MeetsLBYReq && lbyupdated)
			{
				
				pEntity->GetEyeAnglesXY()->y = pEntity->GetLowerBodyYaw();
			}
			else if (!MeetsLBYReq && lbyupdated)
			{
	
				switch (Globals::Shots % 4)
				{
				case 1:
					pEntity->GetEyeAnglesXY()->y = pEntity->GetLowerBodyYaw() - 15;
					break;
				case 2:
					pEntity->GetEyeAnglesXY()->y = pEntity->GetLowerBodyYaw() + 40;
					break;
				case 3:
					pEntity->GetEyeAnglesXY()->y = pEntity->GetLowerBodyYaw() + 15;
					break;
				case 4:
					pEntity->GetEyeAnglesXY()->y = pEntity->GetLowerBodyYaw() - 40;
					break;
				}
			}
			else
				pEntity->GetEyeAnglesXY()->y = rand() % 180 - rand() % 35;
		}

		else if (Globals::missedshots >= 2 && Globals::missedshots <= 3)
		{
			if (MeetsLBYReq && lbyupdated)
			{
				
				pEntity->GetEyeAnglesXY()->y = ResolverSetup::finallbydelta[pEntity->GetIndex()];
			}
			else
				pEntity->GetEyeAnglesXY()->y = ResolverSetup::finaldelta[pEntity->GetIndex()];
		}
		else
		{
			if (MeetsLBYReq && lbyupdated)
			{
				bool timer = 0;
				if (timer)
					pEntity->GetEyeAnglesXY()->y = ResolverSetup::finallbydelta[pEntity->GetIndex()] + rand() % 35;
				else
					pEntity->GetEyeAnglesXY()->y = ResolverSetup::finallbydelta[pEntity->GetIndex()] - rand() % 35;
				timer = !timer;
			}
			else
			{
				bool timer = 0;
				if (timer)
					pEntity->GetEyeAnglesXY()->y = ResolverSetup::finaldelta[pEntity->GetIndex()] + rand() % 35;
				else
					pEntity->GetEyeAnglesXY()->y = ResolverSetup::finaldelta[pEntity->GetIndex()] - rand() % 35;
				timer = !timer;
			}
		}
	}
	PitchCorrection();
}

void ResolverSetup::StoreFGE(IClientEntity* pEntity)
{
	ResolverSetup::storedanglesFGE = pEntity->GetEyeAnglesXY()->y;
	ResolverSetup::storedlbyFGE = pEntity->GetLowerBodyYaw();
	ResolverSetup::storedsimtimeFGE = pEntity->GetSimulationTime();
}

void ResolverSetup::StoreThings(IClientEntity* pEntity)
{
	ResolverSetup::StoredAngles[pEntity->GetIndex()] = *pEntity->GetEyeAnglesXY();
	ResolverSetup::storedlby[pEntity->GetIndex()] = pEntity->GetLowerBodyYaw();
	ResolverSetup::storedsimtime = pEntity->GetSimulationTime();
	ResolverSetup::storeddelta[pEntity->GetIndex()] = pEntity->GetEyeAnglesXY()->y;
	ResolverSetup::storedlby[pEntity->GetIndex()] = pEntity->GetLowerBodyYaw();
}

void ResolverSetup::CM(IClientEntity* pEntity)
{
	for (int x = 1; x < Interfaces::Engine->GetMaxClients(); x++)
	{

		pEntity = (IClientEntity*)Interfaces::EntList->GetClientEntity(x);

		if (!pEntity
			|| pEntity == hackManager.pLocal()
			|| pEntity->IsDormant()
			|| !pEntity->IsAlive())
			continue;

		ResolverSetup::StoreThings(pEntity);
	}
}

void ResolverSetup::FSN(IClientEntity* pEntity, ClientFrameStage_t stage)
{
	if (stage == ClientFrameStage_t::FRAME_NET_UPDATE_POSTDATAUPDATE_START)
	{
		for (int i = 1; i < Interfaces::Engine->GetMaxClients(); i++)
		{

			pEntity = (IClientEntity*)Interfaces::EntList->GetClientEntity(i);

			if (!pEntity
				|| pEntity == hackManager.pLocal()
				|| pEntity->IsDormant()
				|| !pEntity->IsAlive())
				continue;

			ResolverSetup::Resolve(pEntity);
		}
	}
}
/*class PlayerList
{
public:

	IClientEntity * pLocalEz = hackManager.pLocal();
	class CPlayer
	{
	public:
		Vector ShootPos[125];
		int index = -1;
		IClientEntity* entity;
		Vector reset = Vector(0, 0, 0);
		float lastsim = 0;
		Vector lastorigin = Vector(0, 0, 0);
		std::vector< float > pastangles;
		int ScannedNumber = 0;
		int BestIndex = 0;
		float difference = 0.f;
		float Backtrack[360];
		float flLastPelvisAng = 0.f;
		float flEyeAng = 0.f;
		float resolved = 0.f;
		float posedifference = 0.f;
		Hitbox* box;

		CPlayer(IClientEntity* entity, int index, int lastsim) : entity(entity), index(index), lastsim(lastsim)
		{
		}
	};

private:
	std::vector< CPlayer > Players;
public:
	void Update()
	{
		for (int i = 0; i < Players.size(); i++)
		{
			if (Players[i].entity == nullptr)
			{
				Players.erase(Players.begin() + i);
				continue;
			}
			if (Players[i].entity == pLocalEz)
			{
				Players.erase(Players.begin() + i);
				continue;
			};
		}
	}

	void UpdateSim()
	{
		Update();
		for (int i = 0; i < Players.size(); i++)
		{
			Players[i].lastsim = Players[i].entity->GetSimulationTime();
		}
	}

	void AddPlayer(IClientEntity* ent)
	{
		Players.emplace_back(CPlayer(ent, ent->GetIndex(), ent->GetSimulationTime()));
	}

	CPlayer* FindPlayer(IClientEntity* ent)
	{
		for (int i = 0; i < Players.size(); i++)
			if (Players[i].index == ent->GetIndex())
				return &Players[i];
		AddPlayer(ent);
		return FindPlayer(ent);
	}
} plist;




// perfect resolver [censored] tapping
bool backtrackthis;
int velocity;
#define lerpticks rand() / 420 / velocity - 420 * 0

class pEntity
{
public:
	int ViewAngles;
	int Velocity;
	int LBY;
	int EyeAngles;
	bool InAir;
	bool Forceatek;
	int tickcount;
};


void BackTrack()
{
	pEntity* pLocal;
	pEntity* player;
	player->tickcount = player->Velocity / lerpticks * 0 + 420 - player->ViewAngles;
}

bool Baim;
bool HsOnly;
int BruteForce;
bool lbyexposed;
bool abouttoland;
int OldAngles;
float GetEyeAnglesXY;
float UnresolvedAngle[64]; //Can't combine with next statement because otherwise huge {}; with values.
float LBYEyeDelta; //This is for a later resolver that is not mine.
float FinalAngle[64]; //Value to set after resolving is done.
float MovingLBY[64]; //Value to be used if we can't resolve entity.
float StoredLBY[64]; //This will be compared to BrokenLBY.
float BlackListedAngles[64][3]; //A multidimensional array for blacklisting angles, will be reset upon round end.
float BrokenLBY[64]; //Value to be assigned if the entity is fakewalking.
float LowerBodyTargetToSubtract[64]; //This is for a later statement.
bool  BrokenLBYHasValue[64]; //Failsafe to make sure we don't assign the entity's eye angles to a garbage value.
bool  BlackListedAnglesHasValue[64][3]; //This must be multidimensional, as [0][0] might have a value, but [0][1] might not!
bool  ResolvedStatus[64]; //If we log an animation based LBY update, we know for sure that we have resolved the entity (if the entity is using a static angle).
bool  DidWeUseAnimation[64]; //For keeping track of the stupid number of sequence related variables.
static unsigned short BlackListCounter[64]; //This is for keeping track of the second digit in the multi dimensional arrays. Due to the nature of this, only the most recent blacklisted angle will not be shot at, it will suffice though
static unsigned short SequenceCounter[64]; //This is for keeping track of the subtraction of LowerBodyTargetToSubtract.

namespace Menu
{
	namespace Vars
	{
		bool BruteForce;
	}
}
bool onground;
bool lbybroken;




void dab(Vector* & Angle, PlayerList::CPlayer* Player)
{
	pEntity* pLocal;
	pEntity* player;
	Player->reset.y = Angle->y;
	for (int i = 0; i < Interfaces::Engine->GetMaxClients(); ++i)
		if (Menu::Window.RageBotTab.AdvancedResolver.GetState())

			if (player->InAir && !HsOnly)//若人在天上则只爆头
			{
				Baim = true;
			}
			else if (player->InAir && HsOnly)
			{
				if (Menu::Vars::BruteForce)
				{
					BruteForce = true;
					if (Angle->y != Player->resolved)
					{
					}
					else
					{
						if (abouttoland) // about to land reached peak of jump
						{
							pLocal->Forceatek = true; // fire
						}
					}
				}
				else
				{
					onground = true;
				}

				if (lbyexposed)
				{
					pLocal->Forceatek = true;
				}
				else
				{
					int delta = player->ViewAngles - player->LBY;
					if (delta > 29)
					{
						bool lbybroken;
						lbybroken = true;
					}

					if (onground)
					{
						if (player->Velocity > 0)
						{
							player->EyeAngles = player->LBY;
						}
						else
						{
							lbybroken = true;
						}


						if (lbybroken)
						{
							if (Menu::Vars::BruteForce)
							{
								BruteForce = true;
								if (Globals::missedshots >= 1 && Globals::missedshots <= 1)
									if (Angle->y != Player->resolved)
									{
									}
									else
									{
										Sleep(1.1);
										lbyexposed = true;
										player->EyeAngles = OldAngles;
									}
							}


							if (lbyexposed)
							{
								pLocal->Forceatek = true;
							}
							else
							{
								int delta = player->ViewAngles - player->LBY;
								if (delta > 29)
								{
									bool lbybroken;
									lbybroken = true;

								}

								if (lbybroken)
									player->ViewAngles = player->ViewAngles + delta;
							}
						}
					}
				}
			}
}*/