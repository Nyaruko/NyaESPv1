#include "Menu.h"
#include "Controls.h"
#include "Hooks.h" 
#include "Interfaces.h"
#include "CRC32.h"


#define WINDOW_WIDTH 933
#define WINDOW_HEIGHT 585

aristoispejawindow Menu::Window;

void KnifeApplyCallbk()
{
	static ConVar* Meme = Interfaces::CVar->FindVar("cl_fullupdate");
	Meme->nFlags &= ~FCVAR_CHEAT;
	Interfaces::Engine->ClientCmd_Unrestricted("cl_fullupdate");
}

void SaveCallbk()
{
	switch (Menu::Window.ConfigBox.GetIndex())
	{
	case 0:
		GUI.SaveWindowState(&Menu::Window, "legit.cfg");
		break;
	case 1:
		GUI.SaveWindowState(&Menu::Window, "hvh.cfg");
		break;
	case 2:
		GUI.SaveWindowState(&Menu::Window, "hvh2.cfg");
		break;
	case 3:
		GUI.SaveWindowState(&Menu::Window, "hvh3.cfg");
		break;
	case 4:
		GUI.SaveWindowState(&Menu::Window, "other.cfg");
		break;
	}
}

void LoadCallbk()
{
	switch (Menu::Window.ConfigBox.GetIndex())
	{
	case 0:
		GUI.LoadWindowState(&Menu::Window, "legit.cfg");
		break;
	case 1:
		GUI.LoadWindowState(&Menu::Window, "hvh.cfg");
		break;
	case 2:
		GUI.LoadWindowState(&Menu::Window, "hvh2.cfg");
		break;
	case 3:
		GUI.LoadWindowState(&Menu::Window, "hvh3.cfg");
		break;
	case 4:
		GUI.LoadWindowState(&Menu::Window, "other.cfg");
		break;
	}
}


void UnLoadCallbk()
{
	DoUnload = true;
}
void aristoispejawindow::Setup()
{
	SetPosition(350, 50);
	SetSize(WINDOW_WIDTH, WINDOW_HEIGHT);
	SetTitle("SKEET BY NYARUKO");

	RegisterTab(&RageBotTab);
	RegisterTab(&LegitBotTab);
	RegisterTab(&VisualsTab);
	RegisterTab(&SettingsTab);
	RegisterTab(&MiscTab);
	RegisterTab(&ColorsTab);

	RECT Client = GetClientArea();
	Client.bottom -= 29;

	RageBotTab.Setup();
	LegitBotTab.Setup();
	VisualsTab.Setup();
	MiscTab.Setup();
	SettingsTab.Setup();
	ColorsTab.Setup();

#pragma region Bottom Buttons

	ConfigBox.SetFileId("cfg_box");
	ConfigBox.AddItem("演技");
	ConfigBox.AddItem("HvH");
	ConfigBox.AddItem("HvH2");
	ConfigBox.AddItem("HvH3");
	ConfigBox.AddItem("其它");
	ConfigBox.SetSize(112, 200);
	ConfigBox.SetPosition(10, 410); // +350

	SaveButton.SetText("保存");
	SaveButton.SetCallback(SaveCallbk);
	SaveButton.SetSize(112, 200);
	SaveButton.SetPosition(10, 430);

	LoadButton.SetText("加载");
	LoadButton.SetCallback(LoadCallbk);
	LoadButton.SetSize(112, 200);
	LoadButton.SetPosition(10, 460);

	MiscTab.RegisterControl(&SaveButton);
	MiscTab.RegisterControl(&LoadButton);
	MiscTab.RegisterControl(&ConfigBox);

	LegitBotTab.RegisterControl(&SaveButton);
	LegitBotTab.RegisterControl(&LoadButton);
	LegitBotTab.RegisterControl(&ConfigBox);

	RageBotTab.RegisterControl(&SaveButton);
	RageBotTab.RegisterControl(&LoadButton);
	RageBotTab.RegisterControl(&ConfigBox);

	VisualsTab.RegisterControl(&SaveButton);
	VisualsTab.RegisterControl(&LoadButton);
	VisualsTab.RegisterControl(&ConfigBox);

	ColorsTab.RegisterControl(&SaveButton);
	ColorsTab.RegisterControl(&LoadButton);
	ColorsTab.RegisterControl(&ConfigBox);


	SettingsTab.RegisterControl(&SaveButton);
	SettingsTab.RegisterControl(&LoadButton);
	SettingsTab.RegisterControl(&ConfigBox);
	
#pragma endregion
}

void CRageBotTab::Setup()
{
	
	SetTitle("暴力自瞄");

	ActiveLabel.SetPosition(46 + 134, -15 -8);
	ActiveLabel.SetText("激活");
	RegisterControl(&ActiveLabel);

	Active.SetFileId("active");
	Active.SetPosition(86 + 134, -15 -8);
	RegisterControl(&Active);
	
#pragma region Aimbot

	AimbotGroup.SetPosition(16 + 134, -3);
	AimbotGroup.SetText("自瞄");
	AimbotGroup.SetSize(360, 230);
	RegisterControl(&AimbotGroup);

	AimbotEnable.SetFileId("aim_enable");
	AimbotGroup.PlaceCheckBox("启用", this, &AimbotEnable);

	AimbotAutoFire.SetFileId("aim_autofire");
	AimbotGroup.PlaceCheckBox("自动开火", this, &AimbotAutoFire);

	AimbotFov.SetFileId("aim_fov");
	AimbotFov.SetBoundaries(0.f, 180.f);
	AimbotFov.SetValue(39.f);
	AimbotGroup.PlaceOtherControl("瞄准范围", this, &AimbotFov);

	AimbotSilentAim.SetFileId("aim_silent");
	AimbotGroup.PlaceCheckBox("静默", this, &AimbotSilentAim);

	AimbotAimStep.SetFileId("aim_aimstep");
	AimbotGroup.PlaceCheckBox("休闲模式专用", this, &AimbotAimStep);

	AccuracyBacktracking.SetFileId("acc_bcktrk");
	AimbotGroup.PlaceCheckBox("回溯", this, &AccuracyBacktracking);

	AWPAtBody.SetFileId("aim_awpatbody");
	AimbotGroup.PlaceCheckBox("AWP BAIM(打身体)", this, &AWPAtBody);

	AutoRevolver.SetFileId("aim_autorevolver");
	AimbotGroup.PlaceCheckBox("自动左轮", this, &AutoRevolver);

	//AimbotKeyPress.SetFileId("aim_usekey");
	//AimbotGroup.PlaceCheckBox("On Key Press", this, &AimbotKeyPress);

	//AimbotKeyBind.SetFileId("aim_key");
	//AimbotGroup.PlaceOtherControl("Key", this, &AimbotKeyBind);
#pragma endregion Aimbot Controls Get Setup in here

#pragma region Accuracy
	AccuracyGroup.SetPosition(408 + 120, 257 - 8);
	AccuracyGroup.SetText("准确度");
	AccuracyGroup.SetSize(360, 245);
	RegisterControl(&AccuracyGroup);

	AccuracyRecoil.SetFileId("acc_norecoil");
	AccuracyGroup.PlaceCheckBox("移除后座力", this, &AccuracyRecoil);

	AccuracyAutoWall.SetFileId("acc_awall");
	AccuracyGroup.PlaceCheckBox("穿墙", this, &AccuracyAutoWall);

	AccuracyMinimumDamage.SetFileId("acc_mindmg");
	AccuracyMinimumDamage.SetBoundaries(1.f, 99.f);
	AccuracyMinimumDamage.SetValue(1.f);
	AccuracyGroup.PlaceOtherControl("最小伤害", this, &AccuracyMinimumDamage);

	AccuracyAutoScope.SetFileId("acc_scope");
	AccuracyGroup.PlaceCheckBox("自动开镜", this, &AccuracyAutoScope);

	AccuracyAutoStop.SetFileId("acc_stop");
	AccuracyGroup.PlaceCheckBox("自动急停", this, &AccuracyAutoStop);
	AdvancedResolver.SetFileId("acc_advancedresolver");
	//AccuracyGroup.PlaceCheckBox("Pitch修正", this, &AdvancedResolver);
	AdvancedResolver.SetState(false);
	

	AimbotResolver.SetFileId("acc_aaa");
	AimbotResolver.AddItem("关闭");
	AimbotResolver.AddItem("普通");
	AimbotResolver.AddItem("暴力");
	AccuracyGroup.PlaceOtherControl("解析器", this, &AimbotResolver);

	AimbotBaimOnKey.SetFileId("baimonkeypress");
	AccuracyGroup.PlaceOtherControl("bAIM按键", this, &AimbotBaimOnKey);

	AccuracyHitchance.SetFileId("acc_chance");
	AccuracyHitchance.SetBoundaries(0, 100);
	AccuracyHitchance.SetValue(0);
	AccuracyGroup.PlaceOtherControl("命中率", this, &AccuracyHitchance);

	BaimIfUnderXHealth.SetFileId("acc_BaimIfUnderXHealth");
	BaimIfUnderXHealth.SetBoundaries(0, 100);
	BaimIfUnderXHealth.SetValue(25);
	AccuracyGroup.PlaceOtherControl("若HP低于X，则bAIM", this, &BaimIfUnderXHealth);

#pragma endregion  Accuracy controls get Setup in here

#pragma region Target
	TargetGroup.SetPosition(16 + 130, 257 - 8);
	TargetGroup.SetText("目标");
	TargetGroup.SetSize(360, 245);
	RegisterControl(&TargetGroup);

	TargetSelection.SetFileId("tgt_selection");
	TargetSelection.AddItem("最接近准星");
	TargetSelection.AddItem("距离最近");
	TargetSelection.AddItem("HP最低");
	TargetSelection.AddItem("威胁度最高");
	TargetSelection.AddItem("正在开枪的");
	TargetGroup.PlaceOtherControl("目标选择", this, &TargetSelection);

	TargetHitbox.SetFileId("tgt_hitbox");
	TargetHitbox.AddItem("无");
	TargetHitbox.AddItem("头部");
	TargetHitbox.AddItem("脖子");
	TargetHitbox.AddItem("胸部");
	TargetHitbox.AddItem("腹部");
	TargetHitbox.AddItem("全部(低)");
	TargetHitbox.AddItem("全部(中)");
	TargetHitbox.AddItem("全部(高)");
	TargetGroup.PlaceOtherControl("Hitbox", this, &TargetHitbox);

	TargetMultipoint.SetFileId("tgt_multipoint");
	TargetGroup.PlaceCheckBox("边缘瞄准", this, &TargetMultipoint);

	TargetPointscale.SetFileId("tgt_pointscale");
	TargetPointscale.SetBoundaries(0.f, 10.f);
	TargetPointscale.SetValue(5.f);
	TargetGroup.PlaceOtherControl("瞄准高度", this, &TargetPointscale);
#pragma endregion Targetting controls 

#pragma region AntiAim
	AntiAimGroup.SetPosition(408 + 120, -15 -8);
	AntiAimGroup.SetText("反自瞄");
	AntiAimGroup.SetSize(360, 248);
	RegisterControl(&AntiAimGroup);

	AntiAimEnable.SetFileId("aa_enable");
	AntiAimGroup.PlaceCheckBox("启用", this, &AntiAimEnable);

	AntiAimPitch.SetFileId("aa_x");
	AntiAimPitch.AddItem("关");
	AntiAimPitch.AddItem("半低头");
	AntiAimPitch.AddItem("抖动");
	AntiAimPitch.AddItem("低头");
	AntiAimPitch.AddItem("抬头");
	AntiAimPitch.AddItem("平视");
	AntiAimGroup.PlaceOtherControl("纵轴AA", this, &AntiAimPitch);

	AntiAimYaw.SetFileId("aa_y");
	AntiAimYaw.AddItem("关闭");
	AntiAimYaw.AddItem("快速旋转");
	AntiAimYaw.AddItem("慢速旋转");
	AntiAimYaw.AddItem("抖动");
	AntiAimYaw.AddItem("180度抖动");
	AntiAimYaw.AddItem("背身");
	AntiAimYaw.AddItem("背身抖动");
	AntiAimYaw.AddItem("侧身(左右摇摆)");
	AntiAimYaw.AddItem("侧身(右)");
	AntiAimYaw.AddItem("侧身(左)");
	AntiAimYaw.AddItem("LBY");
	AntiAimYaw.AddItem("LBY抖动");
	AntiAimYaw.AddItem("LBY侧身");
	AntiAimYaw.AddItem("LBY旋转");
	AntiAimYaw.AddItem("破坏LBY");
	AntiAimYaw.AddItem("假侧身[ALT]");
	AntiAimYaw.AddItem("假侧身抖动[ALT]");
	AntiAimYaw.AddItem("手动真头切换[←↓→]");
	AntiAimGroup.PlaceOtherControl("Yaw(真身)", this, &AntiAimYaw);

	FakeYaw.SetFileId("fakeaa");
	FakeYaw.AddItem("关闭");
	FakeYaw.AddItem("快速旋转");
	FakeYaw.AddItem("慢速旋转");
	FakeYaw.AddItem("抖动");
	FakeYaw.AddItem("180度抖动");
	FakeYaw.AddItem("背身");
	FakeYaw.AddItem("背身抖动");
	FakeYaw.AddItem("侧身(左右摇摆)");
	FakeYaw.AddItem("侧身(右)");
	FakeYaw.AddItem("侧身(左)");
	FakeYaw.AddItem("LBY");
	FakeYaw.AddItem("LBY抖动");
	FakeYaw.AddItem("LBY侧身");
	FakeYaw.AddItem("LBY旋转");
	//FakeYaw.AddItem("开了就不可信");
	FakeYaw.AddItem("手动假头切换[←↓→]");
	AntiAimGroup.PlaceOtherControl("Yaw(假身)", this, &FakeYaw);

	MoveYaw.SetFileId("aa_y_moving");
	MoveYaw.AddItem("关闭");
	MoveYaw.AddItem("快速旋转");
	MoveYaw.AddItem("慢速旋转");
	MoveYaw.AddItem("抖动");
	MoveYaw.AddItem("180度抖动");
	MoveYaw.AddItem("背身");
	MoveYaw.AddItem("背身抖动");
	MoveYaw.AddItem("侧身(左右摇摆)");
	MoveYaw.AddItem("侧身(右)");
	MoveYaw.AddItem("侧身(左)");
	MoveYaw.AddItem("LBY");
	MoveYaw.AddItem("LBY抖动");
	MoveYaw.AddItem("LBY侧身");
	MoveYaw.AddItem("LBY旋转");
	//MoveYaw.AddItem("手动真头切换[←↓→]");
	AntiAimGroup.PlaceOtherControl("Yaw(真身)(移动中)", this, &MoveYaw);

	MoveYawFake.SetFileId("movingfakeaa");
	MoveYawFake.AddItem("关闭");
	MoveYawFake.AddItem("快速旋转");
	MoveYawFake.AddItem("慢速旋转");
	MoveYawFake.AddItem("抖动");
	MoveYawFake.AddItem("180度抖动");
	MoveYawFake.AddItem("背身");
	MoveYawFake.AddItem("背身抖动");
	MoveYawFake.AddItem("侧身(左右摇摆)");
	MoveYawFake.AddItem("侧身(右)");
	MoveYawFake.AddItem("侧身(左)");
	MoveYawFake.AddItem("LBY");
	MoveYawFake.AddItem("LBY抖动");
	MoveYawFake.AddItem("LBY侧身");
	MoveYawFake.AddItem("LBY旋转");
	//MoveYawFake.AddItem("手动假头切换[←↓→]");
	AntiAimGroup.PlaceOtherControl("Yaw(假身)(移动中)", this, &MoveYawFake);

	BreakLBY.SetFileId("aa_break");
	BreakLBY.AddItem("关");
	BreakLBY.AddItem("45度");
	BreakLBY.AddItem("90度");
	BreakLBY.AddItem("180度");
	//AntiAimGroup.PlaceOtherControl("LBY角度(站立时)", this, &BreakLBY);
	
	BreakLBY.SelectIndex(3);
	//FlipAA.SetFileId("aa_flip");
	//AntiAimGroup.PlaceCheckBox("若被击中则反转AA", this, &FlipAA);
	//FakeWalk.SetFileId("aa_fakewalk");
	//AntiAimGroup.PlaceCheckBox("假走(破坏LBY)(Shift)",this,&FakeWalk);
	AntiAimTarget.SetFileId("aa_target");
	AntiAimGroup.PlaceCheckBox("根据敌人位置改变AA", this, &AntiAimTarget);
	
#pragma endregion  AntiAim controls get setup in here
	
}

void CLegitBotTab::Setup()
{
	
	SetTitle("微自瞄");

	ActiveLabel.SetPosition(46 + 134, -15 - 8);
	ActiveLabel.SetText("激活");
	RegisterControl(&ActiveLabel);

	Active.SetFileId("active");
	Active.SetPosition(86 + 134, -15 - 8);
	RegisterControl(&Active);

#pragma region Aimbot
	AimbotGroup.SetPosition(16 + 134, -3);
	AimbotGroup.SetText("自瞄");
	AimbotGroup.SetSize(240, 220);
	RegisterControl(&AimbotGroup);

	AimbotEnable.SetFileId("aim_enable");
	AimbotGroup.PlaceCheckBox("启用", this, &AimbotEnable);

	AimbotBacktrack.SetFileId("legit_backtrack");
	AimbotGroup.PlaceCheckBox("回溯", this, &AimbotBacktrack);

	TickModulation.SetFileId("tick_modulate");
	TickModulation.SetBoundaries(0.1f, 13.f);
	TickModulation.SetValue(6.f);
	//AimbotGroup.PlaceOtherControl("Tick Modulation", this, &TickModulation);

	AimbotAutoFire.SetFileId("aim_autofire");
	AimbotGroup.PlaceCheckBox("自动开火", this, &AimbotAutoFire);

	AimbotFriendlyFire.SetFileId("aim_friendfire");
	AimbotGroup.PlaceCheckBox("友军伤害", this, &AimbotFriendlyFire);

	AimbotSmokeCheck.SetFileId("otr_smokecheck");
	AimbotGroup.PlaceCheckBox("禁止穿烟", this, &AimbotSmokeCheck);

	AimbotKeyPress.SetFileId("aim_usekey");
	AimbotGroup.PlaceCheckBox("按键自瞄", this, &AimbotKeyPress);

	AimbotKeyBind.SetFileId("aim_key");
	AimbotGroup.PlaceOtherControl("键位", this, &AimbotKeyBind);

#pragma endregion Aimbot shit

#pragma region Triggerbot
	TriggerGroup.SetPosition(272 + 134, -15 - 8);
	TriggerGroup.SetText("自动扳机");
	TriggerGroup.SetSize(240, 220);
	RegisterControl(&TriggerGroup);

	TriggerEnable.SetFileId("trig_enable");
	TriggerGroup.PlaceCheckBox("启用", this, &TriggerEnable);

	TriggerKeyPress.SetFileId("trig_onkey");
	TriggerGroup.PlaceCheckBox("按键激活", this, &TriggerKeyPress);

	TriggerKeyBind.SetFileId("trig_key");
	TriggerGroup.PlaceOtherControl("键位", this, &TriggerKeyBind);

	TriggerRecoil.SetFileId("trig_recoil");
	TriggerGroup.PlaceCheckBox("后座控制", this, &TriggerRecoil);

	TriggerSmokeCheck.SetFileId("trig_smokecheck");
	TriggerGroup.PlaceCheckBox("禁止穿烟", this, &TriggerSmokeCheck);

	TriggerDelay.SetFileId("trig_time");
	TriggerDelay.SetBoundaries(0, 100);
	TriggerDelay.SetValue(1);
	TriggerGroup.PlaceOtherControl("延时", this, &TriggerDelay);

#pragma endregion Triggerbot stuff

#pragma region TriggerbotFilter
	TriggerFilterGroup.SetPosition(528 + 134, -15 - 8);
	TriggerFilterGroup.SetText("过滤器");
	TriggerFilterGroup.SetSize(240, 220);
	RegisterControl(&TriggerFilterGroup);

	TriggerHead.SetFileId("trig_head");
	TriggerFilterGroup.PlaceCheckBox("头部", this, &TriggerHead);

	TriggerChest.SetFileId("trig_chest");
	TriggerFilterGroup.PlaceCheckBox("胸部", this, &TriggerChest);

	TriggerStomach.SetFileId("trig_stomach");
	TriggerFilterGroup.PlaceCheckBox("腹部", this, &TriggerStomach);

	TriggerArms.SetFileId("trig_arms");
	TriggerFilterGroup.PlaceCheckBox("手臂", this, &TriggerArms);

	TriggerLegs.SetFileId("trig_legs");
	TriggerFilterGroup.PlaceCheckBox("腿部", this, &TriggerLegs);

	TriggerTeammates.SetFileId("trig_teammates");
	TriggerFilterGroup.PlaceCheckBox("友军伤害", this, &TriggerTeammates);

#pragma endregion TriggerbotFilter stuff

#pragma region Main Weapon
	WeaponMainGroup.SetPosition(16 + 134, 235);
	WeaponMainGroup.SetText("步枪");
	WeaponMainGroup.SetSize(240, 135);
	RegisterControl(&WeaponMainGroup);

	WeaponMainHitbox.SetFileId("main_hitbox");
	WeaponMainHitbox.AddItem("头部");
	WeaponMainHitbox.AddItem("脖子");
	WeaponMainHitbox.AddItem("胸部");
	WeaponMainHitbox.AddItem("腹部");
	WeaponMainHitbox.AddItem("全部部位");
	WeaponMainGroup.PlaceOtherControl("瞄准部位", this, &WeaponMainHitbox);

	WeaponMainSpeed.SetFileId("main_speed");
	WeaponMainSpeed.SetBoundaries(0.f, 100.f);
	WeaponMainSpeed.SetValue(10.f);
	WeaponMainGroup.PlaceOtherControl("最大速度", this, &WeaponMainSpeed);

	WeaponMainFoV.SetFileId("main_fov");
	WeaponMainFoV.SetBoundaries(0.f, 30.f);
	WeaponMainFoV.SetValue(3.5f);
	WeaponMainGroup.PlaceOtherControl("自瞄范围", this, &WeaponMainFoV);

	WeaponMainRecoil.SetFileId("main_recoil");
	WeaponMainRecoil.SetBoundaries(0.f, 2.f);
	WeaponMainRecoil.SetValue(1.8f);
	WeaponMainGroup.PlaceOtherControl("后座控制", this, &WeaponMainRecoil);

	WeaponMainAimtime.SetValue(0);
	WeaoponMainStartAimtime.SetValue(0);

#pragma endregion

#pragma region Pistols
	WeaponPistGroup.SetPosition(272 + 134, 220);
	WeaponPistGroup.SetText("手枪");
	WeaponPistGroup.SetSize(240, 135);
	RegisterControl(&WeaponPistGroup);

	WeaponPistHitbox.SetFileId("pist_hitbox");
	WeaponPistHitbox.AddItem("头部");
	WeaponPistHitbox.AddItem("脖子");
	WeaponPistHitbox.AddItem("胸部");
	WeaponPistHitbox.AddItem("腹部");
	WeaponPistHitbox.AddItem("全部部位");
	WeaponPistGroup.PlaceOtherControl("自瞄部位", this, &WeaponPistHitbox);

	WeaponPistSpeed.SetFileId("pist_speed");
	WeaponPistSpeed.SetBoundaries(0.f, 100.f);
	WeaponPistSpeed.SetValue(15.0f);
	WeaponPistGroup.PlaceOtherControl("最大速度", this, &WeaponPistSpeed);

	WeaponPistFoV.SetFileId("pist_fov");
	WeaponPistFoV.SetBoundaries(0.f, 30.f);
	WeaponPistFoV.SetValue(3.f);
	WeaponPistGroup.PlaceOtherControl("自瞄范围", this, &WeaponPistFoV);

	WeaponPistRecoil.SetFileId("pist_recoil");
	WeaponPistRecoil.SetBoundaries(0.f, 2.f);
	WeaponPistRecoil.SetValue(2.f);
	WeaponPistGroup.PlaceOtherControl("后座控制", this, &WeaponPistRecoil);

	WeaponPistAimtime.SetValue(0);
	WeaoponPistStartAimtime.SetValue(0);

#pragma endregion

#pragma region Snipers
	WeaponSnipGroup.SetPosition(528 + 134, 220);
	WeaponSnipGroup.SetText("狙击");
	WeaponSnipGroup.SetSize(240, 135);
	RegisterControl(&WeaponSnipGroup);

	WeaponSnipHitbox.SetFileId("snip_hitbox");
	WeaponSnipHitbox.AddItem("头部");
	WeaponSnipHitbox.AddItem("脖子");
	WeaponSnipHitbox.AddItem("胸部");
	WeaponSnipHitbox.AddItem("腹部");
	WeaponSnipHitbox.AddItem("全部部位");
	WeaponSnipGroup.PlaceOtherControl("自瞄部位", this, &WeaponSnipHitbox);

	WeaponSnipSpeed.SetFileId("snip_speed");
	WeaponSnipSpeed.SetBoundaries(0.f, 100.f);
	WeaponSnipSpeed.SetValue(20.f);
	WeaponSnipGroup.PlaceOtherControl("最大速度", this, &WeaponSnipSpeed);

	WeaponSnipFoV.SetFileId("snip_fov");
	WeaponSnipFoV.SetBoundaries(0.f, 30.f);
	WeaponSnipFoV.SetValue(6.f);
	WeaponSnipGroup.PlaceOtherControl("自瞄范围", this, &WeaponSnipFoV);

	WeaponSnipRecoil.SetFileId("snip_recoil");
	WeaponSnipRecoil.SetBoundaries(0.f, 2.f);
	WeaponSnipRecoil.SetValue(1.f);
	WeaponSnipGroup.PlaceOtherControl("后座控制", this, &WeaponSnipRecoil);

	WeaponSnipAimtime.SetValue(0);
	WeaoponSnipStartAimtime.SetValue(0);

#pragma region MPs
	WeaponMpGroup.SetPosition(16 + 134, 383);
	WeaponMpGroup.SetText("微冲");
	WeaponMpGroup.SetSize(240, 136);
	RegisterControl(&WeaponMpGroup);

	WeaponMpHitbox.SetFileId("mps_hitbox");
	WeaponMpHitbox.AddItem("头部");
	WeaponMpHitbox.AddItem("脖子");
	WeaponMpHitbox.AddItem("胸部");
	WeaponMpHitbox.AddItem("腹部");
	WeaponMpHitbox.AddItem("全部部位");
	WeaponMpGroup.PlaceOtherControl("自瞄部位", this, &WeaponMpHitbox);

	WeaponMpSpeed.SetFileId("mps_speed");
	WeaponMpSpeed.SetBoundaries(0.f, 100.f);
	WeaponMpSpeed.SetValue(10.0f);
	WeaponMpGroup.PlaceOtherControl("最大速度", this, &WeaponMpSpeed);

	WeaponMpFoV.SetFileId("mps_fov");
	WeaponMpFoV.SetBoundaries(0.f, 30.f);
	WeaponMpFoV.SetValue(4.f);
	WeaponMpGroup.PlaceOtherControl("自瞄范围", this, &WeaponMpFoV);

	WeaponMpRecoil.SetFileId("mps_recoil");
	WeaponMpRecoil.SetBoundaries(0.f, 2.f);
	WeaponMpRecoil.SetValue(1.6f);
	WeaponMpGroup.PlaceOtherControl("后座控制", this, &WeaponMpRecoil);

	WeaponMpAimtime.SetValue(0);
	WeaoponMpStartAimtime.SetValue(0);
#pragma endregion

#pragma region Shotguns
	WeaponShotgunGroup.SetPosition(272 + 134, 375);
	WeaponShotgunGroup.SetText("霰弹");
	WeaponShotgunGroup.SetSize(240, 136 + 8);
	RegisterControl(&WeaponShotgunGroup);

	WeaponShotgunHitbox.SetFileId("shotgun_hitbox");
	WeaponShotgunHitbox.AddItem("头部");
	WeaponShotgunHitbox.AddItem("脖子");
	WeaponShotgunHitbox.AddItem("胸部");
	WeaponShotgunHitbox.AddItem("腹部");
	WeaponShotgunHitbox.AddItem("全部部位");
	WeaponShotgunGroup.PlaceOtherControl("Hitbox", this, &WeaponShotgunHitbox);

	WeaponShotgunSpeed.SetFileId("shotgun_speed");
	WeaponShotgunSpeed.SetBoundaries(0.f, 100.f);
	WeaponShotgunSpeed.SetValue(20.0f);
	WeaponShotgunGroup.PlaceOtherControl("最大速度", this, &WeaponShotgunSpeed);

	WeaponShotgunFoV.SetFileId("shotgun_fov");
	WeaponShotgunFoV.SetBoundaries(0.f, 30.f);
	WeaponShotgunFoV.SetValue(5.f);
	WeaponShotgunGroup.PlaceOtherControl("自瞄范围", this, &WeaponShotgunFoV);

	WeaponShotgunRecoil.SetFileId("snip_recoil");
	WeaponShotgunRecoil.SetBoundaries(0.f, 2.f);
	WeaponShotgunRecoil.SetValue(1.f);
	WeaponShotgunGroup.PlaceOtherControl("后座控制", this, &WeaponShotgunRecoil);

	WeaponShotgunAimtime.SetValue(0);
	WeaoponShotgunStartAimtime.SetValue(0);

#pragma endregion

#pragma region Machineguns
	WeaponMGGroup.SetPosition(528 + 134, 375);
	WeaponMGGroup.SetText("机枪");
	WeaponMGGroup.SetSize(240, 136 + 8);
	RegisterControl(&WeaponMGGroup);

	WeaponMGHitbox.SetFileId("mg_hitbox");
	WeaponMGHitbox.AddItem("头部");
	WeaponMGHitbox.AddItem("脖子");
	WeaponMGHitbox.AddItem("胸部");
	WeaponMGHitbox.AddItem("腹部");
	WeaponMGHitbox.AddItem("全部部位");
	WeaponMGGroup.PlaceOtherControl("瞄准部位", this, &WeaponMGHitbox);

	WeaponMGSpeed.SetFileId("mg_speed");
	WeaponMGSpeed.SetBoundaries(0.f, 100.f);
	WeaponMGSpeed.SetValue(15.0f);
	WeaponMGGroup.PlaceOtherControl("最大速度", this, &WeaponMGSpeed);

	WeaponMGFoV.SetFileId("mg_fov");
	WeaponMGFoV.SetBoundaries(0.f, 30.f);
	WeaponMGFoV.SetValue(4.f);
	WeaponMGGroup.PlaceOtherControl("自瞄范围", this, &WeaponMGFoV);

	WeaponMGRecoil.SetFileId("mg_recoil");
	WeaponMGRecoil.SetBoundaries(0.f, 2.f);
	WeaponMGRecoil.SetValue(2.f);
	WeaponMGGroup.PlaceOtherControl("后座控制", this, &WeaponMGRecoil);

	WeaponMGAimtime.SetValue(0);
	WeaoponMGStartAimtime.SetValue(0);
	
#pragma endregion
}

void CVisualTab::Setup()
{
	
	SetTitle("视觉");

	ActiveLabel.SetPosition(46 + 134, -15 - 8);
	ActiveLabel.SetText("激活");
	RegisterControl(&ActiveLabel);

	Active.SetFileId("active");
	Active.SetPosition(86 + 134, -15 - 8);
	RegisterControl(&Active);

#pragma region Options
	BoxGroup.SetText("方框");
	BoxGroup.SetPosition(363 - 25, -15 - 8);
	BoxGroup.SetSize(173, 155);
	RegisterControl(&BoxGroup);

	OptionsBox.SetFileId("otr_showbox");
	BoxGroup.PlaceCheckBox("显示方框", this, &OptionsBox);

	OptionsName.SetFileId("opt_name");
	BoxGroup.PlaceCheckBox("名字", this, &OptionsName);

	OptionsWeapon.SetFileId("opt_weapon");
	BoxGroup.PlaceCheckBox("武器", this, &OptionsWeapon);

	OptionHealthEnable.SetFileId("opt_health");
	BoxGroup.PlaceCheckBox("血量", this, &OptionHealthEnable);

	OptionsArmor.SetFileId("opt_armor");
	BoxGroup.PlaceCheckBox("护甲", this, &OptionsArmor);

	OptionsSkeleton.SetFileId("opt_bone");
	BoxGroup.PlaceCheckBox("骨骼", this, &OptionsSkeleton);

	ChamsGroup.SetText("发光");
	ChamsGroup.SetPosition(16 + 134, -3);  //-23
	ChamsGroup.SetSize(173, 135);
	RegisterControl(&ChamsGroup);

	OptionsChams.SetFileId("opt_chams");
	OptionsChams.AddItem("关");
	OptionsChams.AddItem("普通");
	OptionsChams.AddItem("仅贴图");
	ChamsGroup.PlaceOtherControl("Chams", this, &OptionsChams);

	/*OtherNoHands.SetFileId("otr_hands");
	OtherNoHands.AddItem("关");
	OtherNoHands.AddItem("无手臂");
	OtherNoHands.AddItem("半透明");
	OtherNoHands.AddItem("线框");
	OtherNoHands.AddItem("发光");
	OtherNoHands.AddItem("彩虹");
	ChamsGroup.PlaceOtherControl("手臂", this, &OtherNoHands);
	*/
	FakeAngleChams.SetFileId("aa_AAchams");
	FakeAngleChams.AddItem("关");
	FakeAngleChams.AddItem("显示假身");
	FakeAngleChams.AddItem("显示LBY");
	ChamsGroup.PlaceOtherControl("假身显示", this, &FakeAngleChams);
	ChamsVisibleOnly.SetFileId("opt_chamsvisonly");
	//ChamsGroup.PlaceCheckBox("仅可见时发光", this, &ChamsVisibleOnly);
	ChamsVisibleOnly.SetState(true);
	pLocalOpacity.SetFileId("otr_plocal_opacity");
	//ChamsGroup.PlaceCheckBox("不透明度", this, &pLocalOpacity);

	OptionsGroup.SetText("杂项");
	OptionsGroup.SetPosition(150, 155 + 183);
	OptionsGroup.SetSize(173, 155);
	RegisterControl(&OptionsGroup);

	OtherHitmarker.SetFileId("otr_hitmarker");
	OptionsGroup.PlaceCheckBox("命中提示", this, &OtherHitmarker);

	HitmarkerSound.SetFileId("otr_hitmarkersound");
	OptionsGroup.PlaceCheckBox("命中提示音", this, &HitmarkerSound);

	Logs.SetFileId("otr_logs");
	OptionsGroup.PlaceCheckBox("日志", this, &Logs);

	OptionsAimSpot.SetFileId("opt_aimspot");
	OptionsGroup.PlaceCheckBox("头部十字线", this, &OptionsAimSpot);

	BacktrackingLol.SetFileId("opt_backdot");
	OptionsGroup.PlaceCheckBox("回溯", this, &BacktrackingLol);

	InfoGroup.SetText("信息");
	InfoGroup.SetPosition(526, 155);
	InfoGroup.SetSize(173, 155);
	RegisterControl(&InfoGroup);

	OptionsInfo.SetFileId("opt_info");
	InfoGroup.PlaceCheckBox("显示信息", this, &OptionsInfo);

	ResolverInfo.SetFileId("opt_resolverinfo");
	InfoGroup.PlaceCheckBox("显示解析模式", this, &ResolverInfo);

	OptionsCompRank.SetFileId("opt_comprank");
	InfoGroup.PlaceCheckBox("显示水平组", this, &OptionsCompRank);

	RemovalsGroup.SetText("移除选项");
	RemovalsGroup.SetPosition(566 - 40, -15 - 8);
	RemovalsGroup.SetSize(173, 155);
	RegisterControl(&RemovalsGroup);


	OtherNoVisualRecoil.SetFileId("otr_visrecoil");
	RemovalsGroup.PlaceCheckBox("移除视觉后座", this, &OtherNoVisualRecoil);

	OtherNoFlash.SetFileId("otr_noflash");
	RemovalsGroup.PlaceCheckBox("移除闪光", this, &OtherNoFlash);

	OtherNoSmoke.SetFileId("otr_nosmoke");
	RemovalsGroup.PlaceCheckBox("移除烟雾", this, &OtherNoSmoke);

	OtherNoScope.SetFileId("otr_noscope");
	RemovalsGroup.PlaceCheckBox("移除狙击镜", this, &OtherNoScope);

#pragma endregion Setting up the Options controls

#pragma region Filters
	FiltersGroup.SetText("过滤器");
	FiltersGroup.SetPosition(749 - 40, -15 - 8);
	FiltersGroup.SetSize(173, 155);
	RegisterControl(&FiltersGroup);

	//FiltersAll.SetFileId("ftr_all");
	//FiltersGroup.PlaceCheckBox("All", this, &FiltersAll);

	FiltersPlayers.SetFileId("ftr_players");
	FiltersGroup.PlaceCheckBox("显示玩家", this, &FiltersPlayers);
	FiltersPlayers.SetState(true);
	FiltersSelf.SetFileId("ftr_self");
	FiltersGroup.PlaceCheckBox("自己", this, &FiltersSelf);
	

	FiltersEnemiesOnly.SetFileId("ftr_enemyonly");
	FiltersGroup.PlaceCheckBox("仅显示敌人", this, &FiltersEnemiesOnly);
	FiltersEnemiesOnly.SetState(true);
	FiltersWeapons.SetFileId("ftr_weaps");
	FiltersGroup.PlaceCheckBox("武器", this, &FiltersWeapons);

	FiltersNades.SetFileId("ftr_nades");
	FiltersGroup.PlaceCheckBox("道具", this, &FiltersNades);

	FiltersC4.SetFileId("ftr_c4");
	FiltersGroup.PlaceCheckBox("C4", this, &FiltersC4);

#pragma endregion Setting up the Filters controls

#pragma region Other
	OtherGroup.SetText("额外显示");
	OtherGroup.SetPosition(150, 155);
	OtherGroup.SetSize(173, 155);
	RegisterControl(&OtherGroup);

	OtherSpreadCrosshair.SetFileId("otr_spreadhair");
	OtherGroup.PlaceCheckBox("显示武器扩散", this, &OtherSpreadCrosshair);

	SpecList.SetFileId("otr_speclist");
	OtherGroup.PlaceCheckBox("显示观察者", this, &SpecList);

	OtherRadar.SetFileId("otr_radar");
	OtherGroup.PlaceCheckBox("揭示雷达", this, &OtherRadar);

	Watermark.SetFileId("otr_watermark");
	Watermark.SetState(true);
	//OtherGroup.PlaceCheckBox("显示水印", this, &Watermark);

	WelcomeMessage.SetFileId("otr_welcomemsg");
	WelcomeMessage.SetState(true);
	//OtherGroup.PlaceCheckBox("Show Greeting", this, &WelcomeMessage);

	GlowGroup.SetText("轮廓发光");
	GlowGroup.SetPosition(363 - 25, 155);
	GlowGroup.SetSize(173, 155);
	RegisterControl(&GlowGroup);

	OptionsGlow.SetFileId("opt_glow");
	GlowGroup.PlaceCheckBox("轮廓", this, &OptionsGlow);

	EntityGlow.SetFileId("opt_entityglow");
	GlowGroup.PlaceCheckBox("实体轮廓", this, &EntityGlow);

	//GlowZ.SetFileId("opt_glowz");
	GlowZ.SetBoundaries(0.f, 255.f);
	GlowZ.SetValue(255);
	//OptionsGroup.PlaceOtherControl("Glow Alpha", this, &GlowZ);

	RenderGroup.SetText("额外渲染");
	RenderGroup.SetPosition(709, 155);
	RenderGroup.SetSize(173, 155);
	RegisterControl(&RenderGroup);

	Barrels.SetFileId("opt_barrels");
	RenderGroup.PlaceCheckBox("追踪线", this, &Barrels);

	AALines.SetFileId("opt_aalines");
	RenderGroup.PlaceCheckBox("反自瞄线", this, &AALines);

	OtherViewmodelFOV.SetFileId("otr_viewfov");
	OtherViewmodelFOV.SetBoundaries(0.f, 180.f);
	OtherViewmodelFOV.SetValue(0.f);
	RenderGroup.PlaceOtherControl("模型Fov", this, &OtherViewmodelFOV);

	OtherFOV.SetFileId("otr_fov");
	OtherFOV.SetBoundaries(0.f, 70.f);
	OtherFOV.SetValue(0.f);
	RenderGroup.PlaceOtherControl("视觉FOV", this, &OtherFOV);


	
#pragma endregion Setting up the Other controls
}

void CMiscTab::Setup()
{
	
	SetTitle("杂项");

#pragma region Other
	OtherGroup.SetPosition(408 + 124, -15 - 8);
	OtherGroup.SetSize(360, 270);
	OtherGroup.SetText("其它");
	RegisterControl(&OtherGroup);

	OtherSafeMode.SetFileId("otr_safemode");
	OtherSafeMode.SetState(true);
	//OtherGroup.PlaceCheckBox("反不可信", this, &OtherSafeMode);

	OtherThirdperson.SetFileId("aa_thirdpsr");
	OtherGroup.PlaceCheckBox("第三人称", this, &OtherThirdperson);

	//ThirdpersonAngle.SetFileId("aa_thirdpersonMode");
	//ThirdpersonAngle.AddItem("真身");
	//ThirdpersonAngle.AddItem("假身");
	//ThirdpersonAngle.AddItem("LBY");
	//OtherGroup.PlaceOtherControl("第三人称模式", this, &ThirdpersonAngle);
	ThirdpersonAngle.SelectIndex(1);
	ThirdPersonKeyBind.SetFileId("aa_thirdpersonKey");
	OtherGroup.PlaceOtherControl("第三人称热键", this, &ThirdPersonKeyBind);

	AutoPistol.SetFileId("otr_autopistol");
	OtherGroup.PlaceCheckBox("自动手枪", this, &AutoPistol);
	OtherAutoJump.SetFileId("otr_autojump");
	OtherGroup.PlaceCheckBox("连跳", this, &OtherAutoJump);
	//ChatSpamChinese.SetFileId("otr_SpamChinese");
	//OtherGroup.PlaceCheckBox("骂人", this, &ChatSpamChinese);

	OtherAutoStrafe.SetFileId("otr_strafe");
	OtherAutoStrafe.AddItem("关闭");
	OtherAutoStrafe.AddItem("微加速");
	OtherAutoStrafe.AddItem("暴力加速");
	OtherGroup.PlaceOtherControl("空中加速", this, &OtherAutoStrafe);

	NameChanger.SetFileId("otr_spam");
	NameChanger.AddItem("关闭");
	NameChanger.AddItem("Q群改名");
	NameChanger.AddItem("静默改名漏洞");
	NameChanger.AddItem("窃取队友名字");
	NameChanger.AddItem("卡机代码刷屏");
	NameChanger.AddItem("NyaESP改名");
	OtherGroup.PlaceOtherControl("改名", this, &NameChanger);
	//SilentExploit.SetFileId("otr_exploit");
	//OtherGroup.PlaceCheckBox("无限改名漏洞",this,&SilentExploit);
	

	OtherClantag.SetFileId("otr_clantag");
	OtherClantag.AddItem("关闭");
	//OtherClantag.AddItem("aristois");
	//OtherClantag.AddItem("aristois 2");
	//OtherClantag.AddItem("cantmeme.us");
	OtherClantag.AddItem("掩盖名字");
	//OtherClantag.AddItem("best");
	OtherClantag.AddItem("skeet组");
	OtherClantag.AddItem("Q群");
	OtherClantag.AddItem("自定义");
	OtherClantag.AddItem("本地时间");
	OtherGroup.PlaceOtherControl("组名", this, &OtherClantag);

	customclan.SetFileId("otr_customclan");
	customclan.SetText("对面5个SB");
	OtherGroup.PlaceOtherControl("自定义组名", this, &customclan);

	OtherAirStuck.SetFileId("otr_astuck");
	OtherGroup.PlaceOtherControl("空中悬浮", this, &OtherAirStuck);

#pragma endregion other random options

#pragma region FakeLag
	FakeLagGroup.SetPosition(16 + 134, -15 - 8);
	FakeLagGroup.SetSize(360, 105);
	FakeLagGroup.SetText("假延迟");
	RegisterControl(&FakeLagGroup);

	FakeLagEnable.SetFileId("fakelag_enable");
	FakeLagGroup.PlaceCheckBox("启用", this, &FakeLagEnable);

	FakeLagChoke.SetFileId("fakelag_choke");
	FakeLagChoke.SetBoundaries(0, 18);
	FakeLagChoke.SetValue(4);
	FakeLagGroup.PlaceOtherControl("阻塞值", this, &FakeLagChoke);

	FakeLagTyp.SetFileId("fakelag_typ");
	FakeLagTyp.AddItem("关闭");
	FakeLagTyp.AddItem("普通");
	FakeLagTyp.AddItem("适应性");
	FakeLagTyp.AddItem("普通(仅空中激活)");
	FakeLagTyp.AddItem("适应性(仅空中激活)");
	FakeLagTyp.AddItem("破坏延迟补偿");
	FakeLagGroup.PlaceOtherControl("模式", this, &FakeLagTyp);

#pragma endregion fakelag shit

#pragma region Buybot
	BuyBotGroup.SetPosition(16 + 134, 105 - 8);
	BuyBotGroup.SetSize(360, 150);
	BuyBotGroup.SetText("自动购买");
	RegisterControl(&BuyBotGroup);

	EnableBuyBot.SetFileId("bb_enable");
	BuyBotGroup.PlaceCheckBox("启用", this, &EnableBuyBot);

	BuyBot.SetFileId("buybot");
	BuyBot.AddItem("关闭");
	BuyBot.AddItem("Ak/M4");
	BuyBot.AddItem("AWP");
	BuyBot.AddItem("连狙+双枪");
	BuyBotGroup.PlaceOtherControl("自动购买", this, &BuyBot);

	BuyBotGrenades.SetFileId("buybot_grenades");
	BuyBotGrenades.AddItem("关闭");
	BuyBotGrenades.AddItem("烟 闪 雷");
	BuyBotGrenades.AddItem("烟 雷 火+闪");
	BuyBotGroup.PlaceOtherControl("自动买雷", this, &BuyBotGrenades);

	BuyBotKevlar.SetFileId("buybot_kevlar");
	BuyBotGroup.PlaceCheckBox("自动买甲", this, &BuyBotKevlar);

	BuyBotDefuser.SetFileId("buybot_defuser");
	BuyBotGroup.PlaceCheckBox("自动买钳", this, &BuyBotDefuser);

	KnifeGroup.SetPosition(408 + 124, 270);
	KnifeGroup.SetText("皮肤修改器");
	KnifeGroup.SetSize(360, 100);
	RegisterControl(&KnifeGroup);

	SkinEnable.SetFileId("otr_skinenable");
	KnifeGroup.PlaceCheckBox("启用", this, &SkinEnable);

	KnifeModel.SetFileId("knife_model");
	KnifeModel.AddItem("刺刀");
	KnifeModel.AddItem("鲍伊猎刀");
	KnifeModel.AddItem("蝴蝶刀");
	KnifeModel.AddItem("弯刀");
	KnifeModel.AddItem("折叠刀");
	KnifeModel.AddItem("穿肠刀");
	KnifeModel.AddItem("杀猪刀");
	KnifeModel.AddItem("爪子刀");
	KnifeModel.AddItem("M9刺刀");
	KnifeModel.AddItem("暗影双匕");
	KnifeGroup.PlaceOtherControl("刀", this, &KnifeModel);
	
	KnifeSkin.SetFileId("knife_skin");
	KnifeGroup.PlaceOtherControl("皮肤ID", this, &KnifeSkin);

	KnifeApply.SetText("应用修改");
	KnifeApply.SetCallback(KnifeApplyCallbk);
	KnifeApply.SetPosition(408 + 135, 400);
	KnifeApply.SetSize(360, 106);
	RegisterControl(&KnifeApply);

#pragma endregion Buybot
	
}

void CSettingsTab::Setup()
{
	
	SetTitle("设置");

	wpnhitchanceGroup.SetPosition(16 + 134, -15 - 8);
	wpnhitchanceGroup.SetSize(360, 190);
	wpnhitchanceGroup.SetText("命中率");
	RegisterControl(&wpnhitchanceGroup);

	WeaponCheck.SetFileId("weapon_chcek");
	wpnhitchanceGroup.PlaceCheckBox("启用", this, &WeaponCheck);

	scoutChance.SetFileId("scout_chance");
	scoutChance.SetBoundaries(0, 100);
	scoutChance.SetValue(82);
	wpnhitchanceGroup.PlaceOtherControl("鸟狙", this, &scoutChance);

	AWPChance.SetFileId("awp_chance");
	AWPChance.SetBoundaries(0, 100);
	AWPChance.SetValue(60);
	wpnhitchanceGroup.PlaceOtherControl("AWP", this, &AWPChance);

	AutoChance.SetFileId("auto_chance");
	AutoChance.SetBoundaries(0, 100);
	AutoChance.SetValue(40);
	wpnhitchanceGroup.PlaceOtherControl("连狙", this, &AutoChance);

	RifleChance.SetFileId("rifle_chance");
	RifleChance.SetBoundaries(0, 100);
	RifleChance.SetValue(10);
	wpnhitchanceGroup.PlaceOtherControl("步枪", this, &RifleChance);

	MPChance.SetFileId("mp_chance");
	MPChance.SetBoundaries(0, 100);
	MPChance.SetValue(5);
	wpnhitchanceGroup.PlaceOtherControl("冲锋枪", this, &MPChance);

	PistolChance.SetFileId("pistol_chance");
	PistolChance.SetBoundaries(0, 100);
	PistolChance.SetValue(20);
	wpnhitchanceGroup.PlaceOtherControl("手枪", this, &PistolChance);



	wpnmindmgGroup.SetPosition(408 + 124, -15 - 8);
	wpnmindmgGroup.SetSize(360, 190);
	wpnmindmgGroup.SetText("最小伤害");
	RegisterControl(&wpnmindmgGroup);

	scoutmindmg.SetFileId("scout_mindmg");
	scoutmindmg.SetBoundaries(0, 100);
	scoutmindmg.SetValue(15);
	wpnmindmgGroup.PlaceOtherControl("鸟狙", this, &scoutmindmg);

	AWPmindmg.SetFileId("awp_mindmg");
	AWPmindmg.SetBoundaries(0, 100);
	AWPmindmg.SetValue(15);
	wpnmindmgGroup.PlaceOtherControl("AWP", this, &AWPmindmg);

	Automindmg.SetFileId("auto_mindmg");
	Automindmg.SetBoundaries(0, 100);
	Automindmg.SetValue(15);
	wpnmindmgGroup.PlaceOtherControl("连狙", this, &Automindmg);

	Riflemindmg.SetFileId("rifle_mindmg");
	Riflemindmg.SetBoundaries(0, 100);
	Riflemindmg.SetValue(15);
	wpnmindmgGroup.PlaceOtherControl("步枪", this, &Riflemindmg);

	MPmindmg.SetFileId("mp_mindmg");
	MPmindmg.SetBoundaries(0, 100);
	MPmindmg.SetValue(15);
	wpnmindmgGroup.PlaceOtherControl("冲锋枪", this, &MPmindmg);

	Pistolmindmg.SetFileId("pistol_mindmg");
	Pistolmindmg.SetBoundaries(0, 100);
	Pistolmindmg.SetValue(15);
	wpnmindmgGroup.PlaceOtherControl("手枪", this, &Pistolmindmg);
	
};


void CColorTab::Setup()
{
	
	SetTitle("颜色");
#pragma region Visual Colors


	CTVisibleGroup.SetPosition(16 + 134, -23);
	CTVisibleGroup.SetText("CT方框颜色(如可见)");
	CTVisibleGroup.SetSize(170, 110);
	RegisterControl(&CTVisibleGroup);

	CTColorVisR.SetFileId("ct_color_vis_r");
	CTColorVisR.SetBoundaries(0.f, 255.f);
	CTColorVisR.SetValue(0.f);
	CTVisibleGroup.PlaceOtherControl("红色值", this, &CTColorVisR);

	CTColorVisG.SetFileId("ct_color_vis_g");
	CTColorVisG.SetBoundaries(0.f, 255.f);
	CTColorVisG.SetValue(230.f);
	CTVisibleGroup.PlaceOtherControl("绿色值", this, &CTColorVisG);

	CTColorVisB.SetFileId("ct_color_vis_b");
	CTColorVisB.SetBoundaries(0.f, 255.f);
	CTColorVisB.SetValue(15.f);
	CTVisibleGroup.PlaceOtherControl("蓝色值", this, &CTColorVisB);

	CTBoxNotVisGroup.SetPosition(272 + 134 - 70, -23);
	CTBoxNotVisGroup.SetText("CT方框颜色(如不可见)");
	CTBoxNotVisGroup.SetSize(170, 110);
	RegisterControl(&CTBoxNotVisGroup);

	CTColorNoVisR.SetFileId("ct_color_no_vis_r");
	CTColorNoVisR.SetBoundaries(0.f, 255.f);
	CTColorNoVisR.SetValue(0.f);
	CTBoxNotVisGroup.PlaceOtherControl("红色值", this, &CTColorNoVisR);

	CTColorNoVisG.SetFileId("ct_color_no_vis_g");
	CTColorNoVisG.SetBoundaries(0.f, 255.f);
	CTColorNoVisG.SetValue(235.f);
	CTBoxNotVisGroup.PlaceOtherControl("绿色值", this, &CTColorNoVisG);

	CTColorNoVisB.SetFileId("ct_color_no_vis_b");
	CTColorNoVisB.SetBoundaries(0.f, 255.f);
	CTColorNoVisB.SetValue(10.f);
	CTBoxNotVisGroup.PlaceOtherControl("蓝色值", this, &CTColorNoVisB);

	TVisibleBoxGroup.SetPosition(521, -23);
	TVisibleBoxGroup.SetText("T方框颜色（如可见）");
	TVisibleBoxGroup.SetSize(170, 110);
	RegisterControl(&TVisibleBoxGroup);

	TColorVisR.SetFileId("t_color_vis_r");
	TColorVisR.SetBoundaries(0.f, 255.f);
	TColorVisR.SetValue(0.f);
	TVisibleBoxGroup.PlaceOtherControl("红色值", this, &TColorVisR);

	TColorVisG.SetFileId("t_color_vis_g");
	TColorVisG.SetBoundaries(0.f, 255.f);
	TColorVisG.SetValue(50.f);
	TVisibleBoxGroup.PlaceOtherControl("绿色值", this, &TColorVisG);

	TColorVisB.SetFileId("t_color_vis_b");
	TColorVisB.SetBoundaries(0.f, 255.f);
	TColorVisB.SetValue(220.f);
	TVisibleBoxGroup.PlaceOtherControl("蓝色值", this, &TColorVisB);

	Tboxnotvisiblegroup.SetPosition(521 + 184, -23);
	Tboxnotvisiblegroup.SetText("T方框颜色（如不可见）");
	Tboxnotvisiblegroup.SetSize(170, 110);
	RegisterControl(&Tboxnotvisiblegroup);

	TColorNoVisR.SetFileId("t_color_no_vis_r");
	TColorNoVisR.SetBoundaries(0.f, 255.f);
	TColorNoVisR.SetValue(0.f);
	Tboxnotvisiblegroup.PlaceOtherControl("", this, &TColorNoVisR);

	TColorNoVisG.SetFileId("t_color_no_vis_g");
	TColorNoVisG.SetBoundaries(0.f, 255.f);
	TColorNoVisG.SetValue(50.f);
	Tboxnotvisiblegroup.PlaceOtherControl("", this, &TColorNoVisG);

	TColorNoVisB.SetFileId("t_color_no_vis_b");
	TColorNoVisB.SetBoundaries(0.f, 255.f);
	TColorNoVisB.SetValue(220.f);
	Tboxnotvisiblegroup.PlaceOtherControl("", this, &TColorNoVisB);

	GlowGroup.SetPosition(521, 112);
	GlowGroup.SetText("Glow");
	GlowGroup.SetSize(170, 110);
	RegisterControl(&GlowGroup);

	GlowR.SetFileId("GlowR");
	GlowR.SetBoundaries(0.f, 255.f);
	GlowR.SetValue(230.f);
	GlowGroup.PlaceOtherControl("", this, &GlowR);

	GlowG.SetFileId("GlowG");
	GlowG.SetBoundaries(0.f, 255.f);
	GlowG.SetValue(0.f);
	GlowGroup.PlaceOtherControl("", this, &GlowG);

	GlowB.SetFileId("GlowB");
	GlowB.SetBoundaries(0.f, 255.f);
	GlowB.SetValue(0.f);
	GlowGroup.PlaceOtherControl("", this, &GlowB);

	CTCHamsGroup.SetPosition(16 + 134, 112);
	CTCHamsGroup.SetText("CT Chams");
	CTCHamsGroup.SetSize(170, 110);
	RegisterControl(&CTCHamsGroup);

	CTChamsR.SetFileId("ctchamsr");
	CTChamsR.SetBoundaries(0.f, 255.f);
	CTChamsR.SetValue(40.f);
	CTCHamsGroup.PlaceOtherControl("", this, &CTChamsR);

	CTChamsG.SetFileId("ctchamsg");
	CTChamsG.SetBoundaries(0.f, 255.f);
	CTChamsG.SetValue(120.f);
	CTCHamsGroup.PlaceOtherControl("", this, &CTChamsG);

	CTChamsB.SetFileId("ctchamsb");
	CTChamsB.SetBoundaries(0.f, 255.f);
	CTChamsB.SetValue(170.f);
	CTCHamsGroup.PlaceOtherControl("", this, &CTChamsB);

	TChamsGroup.SetPosition(272 + 64, 112);
	TChamsGroup.SetText("T Chams");
	TChamsGroup.SetSize(170, 110);
	RegisterControl(&TChamsGroup);

	TChamsR.SetFileId("tchamsr");
	TChamsR.SetBoundaries(0.f, 255.f);
	TChamsR.SetValue(210.f);
	TChamsGroup.PlaceOtherControl("", this, &TChamsR);

	TChamsG.SetFileId("tchamsg");
	TChamsG.SetBoundaries(0.f, 255.f);
	TChamsG.SetValue(20.f);
	TChamsGroup.PlaceOtherControl("", this, &TChamsG);

	TChamsB.SetFileId("tchamsb");
	TChamsB.SetBoundaries(0.f, 255.f);
	TChamsB.SetValue(70.f);
	TChamsGroup.PlaceOtherControl("", this, &TChamsB);

	MenuColorGroup.SetPosition(521 + 184, 112);
	MenuColorGroup.SetText("Menu");
	MenuColorGroup.SetSize(170, 110);
	RegisterControl(&MenuColorGroup);

	ColorMenuAccentR.SetFileId("menuaccr");
	ColorMenuAccentR.SetBoundaries(0, 255);
	ColorMenuAccentR.SetValue(16);
	MenuColorGroup.PlaceOtherControl("", this, &ColorMenuAccentR);

	ColorMenuAccentG.SetFileId("menuaccg");
	ColorMenuAccentG.SetBoundaries(0, 255);
	ColorMenuAccentG.SetValue(123);
	MenuColorGroup.PlaceOtherControl("", this, &ColorMenuAccentG);

	ColorMenuAccentB.SetFileId("menuaccb");
	ColorMenuAccentB.SetBoundaries(0, 255);
	ColorMenuAccentB.SetValue(255);
	MenuColorGroup.PlaceOtherControl("", this, &ColorMenuAccentB);
	
#pragma endregion
}
void Menu::SetupMenu()
{
	
	Window.Setup();

	GUI.RegisterWindow(&Window);
	GUI.BindWindow(VK_INSERT, &Window);
	
	
}

void Menu::DoUIFrame()
{

	GUI.Update();
	GUI.Draw();
	
}